package net.blueshell.api.business.user;

public enum ResetType {
    INITIAL_ACCOUNT_CREATION,
    PASSWORD_RESET
}
